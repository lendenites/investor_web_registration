package com.invregweb;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.investorregmain.InvestorRegistrationWebTest;

public class BankDetails 
{
	public void page007BankDetails()
	{
		InvestorRegistrationWebTest.driver.findElement(By.cssSelector("input[placeholder='Account holder name']")).sendKeys("Saurabh Sharma");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		InvestorRegistrationWebTest.driver.findElement(By.cssSelector("input[placeholder='Bank a/c number']")).sendKeys("76543823338765");
		InvestorRegistrationWebTest.driver.findElement(By.cssSelector("input[placeholder='Confirm bank a/c number']")).sendKeys("76543823338765");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[2]/div[4]/div")).click();
		InvestorRegistrationWebTest.driver.findElement(By.cssSelector("input[placeholder='Confirm bank a/c number']")).sendKeys(Keys.TAB,Keys.ENTER,Keys.ARROW_DOWN,Keys.ENTER);
		//InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[2]/div[4]/div")).sendKeys(Keys.TAB,Keys.ENTER,Keys.ARROW_DOWN,Keys.ENTER);
		InvestorRegistrationWebTest.driver.findElement(By.cssSelector("input[placeholder='IFSC Code']")).sendKeys("UBIN0547026");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		try {
			System.out.println("inside a button clickable1");
			InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[2]/div[9]/button")).click();
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("2");
			InvestorRegistrationWebTest.driver.findElement(By.className("green-btn")).click();
		}
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		InvestorRegistrationWebTest.driver.findElement(By.className("proceed-btn")).click();



	}
}
