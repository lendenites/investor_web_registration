package com.invregweb;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.investorregmain.InvestorRegistrationWebTest;
import com.lendenclub.data.ExcelSheet;

public class Login
{
//	String Mobnum="9769508657";
//	String pancard="JVUPS4433A";
//	String Email="saurabh0799@gmail.ldc";
	
	public void page001Login() throws InterruptedException 
	{	
		InvestorRegistrationWebTest.driver.get("https://qa-app.lendenclub.com/");
		InvestorRegistrationWebTest.driver.manage().window().maximize();
		WebElement signup=InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/div[5]/span/a"));
		signup.click();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement un=InvestorRegistrationWebTest.driver.findElement(By.name("full_name"));
		un.click();
		un.sendKeys(ExcelSheet.ReadCell(ExcelSheet.GetCell("Investor_Details"), 1));
//		WebElement un=InvestorRegistrationWebTest.driver.findElement(By.name("full_name"));
//	    un.click();
//	    un.sendKeys(ExcelSheet.ReadCell(ExcelSheet.GetCell("Investor_Details"), 1));
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);

		WebElement email=InvestorRegistrationWebTest.driver.findElement(By.name("email"));
		email.click();
		email.sendKeys(ExcelSheet.ReadCell(ExcelSheet.GetCell("Investor_Details"), 2));
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);

		WebElement mobile=InvestorRegistrationWebTest.driver.findElement(By.name("mobile_number"));
		mobile.click();
		mobile.sendKeys(ExcelSheet.ReadCell(ExcelSheet.GetCell("Investor_Details"), 3));
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);
		WebElement otp=InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/div[7]/button"));
		otp.click();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);		//Open new windows
		((JavascriptExecutor) InvestorRegistrationWebTest.driver).executeScript("window.open()");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);

		ArrayList<String>tabs = new ArrayList<String>(InvestorRegistrationWebTest.driver.getWindowHandles());
		InvestorRegistrationWebTest.driver.switchTo().window(tabs.get(1));
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        Thread.sleep(2000);

		//django login for otp
		InvestorRegistrationWebTest.driver.get("https://qa.lendenclub.com/admin/lendenapp/userotp/?q=");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);

		InvestorRegistrationWebTest.driver.findElement(By.name("username")).sendKeys("rakesh.s@lendenclub.ldc");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);

		InvestorRegistrationWebTest.driver.findElement(By.name("password")).sendKeys("lenDen548#$");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"login-form\"]/div[3]/input")).click();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);

		InvestorRegistrationWebTest.driver.findElement(By.name("q")).sendKeys(ExcelSheet.ReadCell(ExcelSheet.GetCell("Investor_Details"), 3));
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Thread.sleep(2000);

		InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"changelist-search\"]/div/input[2]")).click();

		//*[@id="changelist-search"]/div/input[2]
		//variable Declaration
		String text=InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"result_list\"]/tbody/tr/th/a")).getText();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);	
		System.out.println("otp is :"+text);
        Thread.sleep(10000);

		//switching tabs
		InvestorRegistrationWebTest.driver.switchTo().window(tabs.get(0));
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		//Enter otp
		InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"standard-full-width\"]")).sendKeys(text);
		
		InvestorRegistrationWebTest.driver.findElement(By.className("green-btn")).click();
		Thread.sleep(60000);
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

}
