package com.lendenclub.data;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.investorregmain.InvestorRegistrationWebTest;

public class Screenshot implements ITestListener 
{
public static ExcelSheet excel;
	
	@Override
    public void onTestStart(ITestResult iTestResult) {
	
	}

    @Override
    public void onTestSuccess(ITestResult iTestResult) {}

    @Override
    public void onTestFailure(ITestResult iTestResult) {
    	
    	try {
	        
		//Date format for screenshot file name
	       SimpleDateFormat df=new  SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
	       Date date =new Date();
	       String fileName = df.format(date);
	       File file = ((TakesScreenshot)InvestorRegistrationWebTest.getDriver()).getScreenshotAs(OutputType.FILE);
	        //copy screenshot file into screenshot folder.
	        FileUtils.copyFile(file, new File(System.getProperty("user.dir")+"//Screenshot//"+fileName+".jpg"));
	        System.out.println("Screenshot is Captured");
	       
	        
		
    	}catch(Exception e) {
			e.printStackTrace();
		}
    	
    	
    }
    
	@Override
    public void onTestSkipped(ITestResult iTestResult) {}

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {}

    @Override
    public void onStart(ITestContext iTestContext) {
    	
    	try {
			ExcelSheet excel = new ExcelSheet("./excel/ExcelSheet.xls");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	File source = new File("./Screenshot");
		try {
			FileUtils.cleanDirectory(source);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
    }

    @Override
    public void onFinish(ITestContext iTestContext) {}
    public ExcelSheet getExcel()
	{
		return excel;
	}


	
}
